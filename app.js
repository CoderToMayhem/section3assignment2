const path = require('path');
const express = require('express');

const routes = require('./routes');

const app = express();

//Use the static files
app.use(express.static(path.join(__dirname, 'public')));

//Use the configured routes
app.use(routes);

app.listen(2200);