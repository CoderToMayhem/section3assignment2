const path = require('path');
const express = require('express');

const router = express.Router();

router.get('/users', (req, res, next) => {
    res.status(200).sendFile(path.join(__dirname, 'views', 'users.html'));
});

router.get('/', (req, res, next) => {
    res.status(200).sendFile(path.join(__dirname, 'views', 'welcome.html'));
});


module.exports = router;